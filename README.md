# This project is archived. Source has moved to https://codeberg.org/dogbold/grg.


# grg - a TUI for git repos

This script started its life in my dotfiles, but I realized it would be a good idea to release it.

## Usage

```sh
grg <directory> # optional, grg can open in $PWD
```

GRG opens a git repo in your terminal file manager of choice, currently ranger or joshuto.

Once you leave that file manager, we check for all changed files, automatically add and commit them,
then rebase interactively, back to either your repo's remote HEAD, or to a tag, in the case of local repos.

Once you leave the rebase menu, we ask if you want to fully rebase, and if you say no, we prompt for various reset methods, including going back to the rebase menu.

In order to use grg, you will need $TFM set in your shell rc.

```sh
export TFM="joshuto"
```

## Installation

### Any distro

Simply clone the repo, cd in, and `install` it to somewhere in your $PATH.
```sh
git clone https://gitlab.com/beethoven__/grg.git
cd grg
install -Dm755 ~/.local/bin/grg
```

To uninstall, simply delete the file from where you installed it to.

### Arch Linux

grg is available on the AUR, so all you need to do is use an AUR helper.

`yay -S grg-git`
or
`paru -S grg-git`

To uninstall, simply do:

`yay -Rns grg-git`
or
`paru -Rns grg-git`

A few dependencies are needed right now. You need at least:
- [bat, a cat clone with wings](https://github.com/sharkdp/bat)
- [GNU Bash](https://www.gnu.org/software/bash/)
- [Git](https://github.com/git/git)

You may choose between these file managers:
- [ranger, A VIM-inspired filemanager for the console](https://github.com/ranger/ranger)
- [joshuto, ranger-like terminal file manager written in Rust](https://github.com/kamiyaa/joshuto/)

You probably also want a text editor, but I leave that up to you.
